package fr.cyril_cauchois.sailregattatimer;

import android.content.Intent;
import android.os.Bundle;
import android.support.wearable.activity.ConfirmationActivity;
import android.support.wearable.activity.WearableActivity;
import android.support.wearable.view.WatchViewStub;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends WearableActivity {


    private static final String TAG = "RegattaTimer";

    private long mStartTime = 0L;
    private long mPauseTime = 0L;

    private static final long ONE_SECOND = 1000;
    private static final long ONE_MINUTE = 60 * ONE_SECOND;

    private static final long START_PROCEDURE_DURATION = 5 * ONE_MINUTE; // TODO: 11/06/2015 put in settings

    private enum State {STOPPED, COUNTDOWN, COUNTDOWN_PAUSED, COUNT, COUNT_ENDED};

    private State mState = State.STOPPED;
    private TextView mTimerTV;
    private ImageButton mStartBt;
    private ImageButton mPrevBt;
    private ImageButton mNextBt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        Log.d(TAG, "onCreate()");

        super.onCreate(savedInstanceState);

        setAmbientEnabled();
        setContentView(R.layout.activity_main);

        final WatchViewStub stub = (WatchViewStub) findViewById(R.id.watch_view_stub);
        stub.setOnLayoutInflatedListener(new WatchViewStub.OnLayoutInflatedListener() {
            @Override
            public void onLayoutInflated(WatchViewStub stub) {
                mTimerTV = (TextView) stub.findViewById(R.id.main_activity_time_tv);

                mStartBt = (ImageButton) stub.findViewById(R.id.main_activity_start_bt);
                mStartBt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onStartBtClick();
                    }
                });

                mNextBt = (ImageButton) stub.findViewById(R.id.main_activity_next_bt);
                mNextBt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onNextBtClick();
                    }
                });
                mPrevBt = (ImageButton) stub.findViewById(R.id.main_activity_prev_bt);
                mPrevBt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onPrevBtClick();
                    }
                });

                updateUI();
            }
        });
    }


    public void updateUI() {

        if (mTimerTV == null) {
            return; // layout is not ready
        }

        final long time = getTime();
        mTimerTV.setTextColor(TimeFormatter.getColor(time, isAmbient()));
        if (mStartTime != 0) {
            mTimerTV.setText(TimeFormatter.formatHMS(time, isAmbient()));
        }

        if (isAmbient()) {
            mTimerTV.getPaint().setAntiAlias(false);

            mStartBt.setVisibility(View.GONE);
            mPrevBt.setVisibility(View.GONE);
            mNextBt.setVisibility(View.GONE);
        } else {

            mTimerTV.getPaint().setAntiAlias(true);

            switch (mState) {

                case STOPPED: {
                    mStartBt.setVisibility(View.VISIBLE);
                    mStartBt.setImageResource(R.drawable.ic_play_arrow_black_48dp);
                    mPrevBt.setVisibility(View.GONE);
                    mNextBt.setVisibility(View.GONE);
                    break;
                }

                case COUNTDOWN: {
                    mStartBt.setVisibility(View.VISIBLE);
                    mStartBt.setImageResource(R.drawable.ic_pause_black_48dp);
                    mPrevBt.setVisibility(View.VISIBLE);
                    mNextBt.setVisibility(View.VISIBLE);
                    break;
                }

                case COUNTDOWN_PAUSED: {
                    mStartBt.setVisibility(View.VISIBLE);
                    mStartBt.setImageResource(R.drawable.ic_play_arrow_black_48dp);
                    mPrevBt.setVisibility(View.GONE);
                    mNextBt.setVisibility(View.GONE);
                    break;
                }

                case COUNT: {
                    mStartBt.setVisibility(View.VISIBLE);
                    mStartBt.setImageResource(R.drawable.ic_stop_black_48dp);
                    mPrevBt.setVisibility(View.GONE);
                    mNextBt.setVisibility(View.GONE);
                    break;
                }

                case COUNT_ENDED: {
                    mStartBt.setVisibility(View.GONE);
                    mPrevBt.setVisibility(View.GONE);
                    mNextBt.setVisibility(View.GONE);
                    break;
                }
            }


        }


    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.d(TAG, "onSaveInstanceState");

        outState.putLong("startTime", mStartTime);
        outState.putString("state", mState.name());

    }


    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        Log.d(TAG, "onRestoreInstanceState()");
        super.onRestoreInstanceState(savedInstanceState);

        if (savedInstanceState != null) {
            mStartTime = savedInstanceState.getLong("startTime");
            Log.d(TAG, "onRestoreInstanceState() restored startTime : " + mStartTime);
            mState = State.valueOf(savedInstanceState.getString("state"));
            updateUI();
            scheduleRefresh();
        }
    }

    private void onStartBtClick() {

        switch (mState) {

            case STOPPED: {
                mState = State.COUNTDOWN;
                mStartTime = System.currentTimeMillis() + START_PROCEDURE_DURATION;
                updateUI();
                scheduleRefresh();
                break;
            }

            case COUNTDOWN: {
                mState = State.COUNTDOWN_PAUSED;
                mPauseTime = System.currentTimeMillis();
                mTimer.cancel();
                updateUI();
                break;
            }

            case COUNTDOWN_PAUSED: {
                mState = State.COUNTDOWN;
                mStartTime = mStartTime + (System.currentTimeMillis() - mPauseTime);
                mPauseTime = 0L;
                updateUI();
                scheduleRefresh();
                break;
            }

            case COUNT: {
                mState = State.COUNT_ENDED;
                mPauseTime = System.currentTimeMillis();
                mTimer.cancel();
                updateUI();
                showSuccess();
                break;
            }
        }


    }

    private void showSuccess() {
        Intent intent = new Intent(this, ConfirmationActivity.class);
        intent.putExtra(ConfirmationActivity.EXTRA_ANIMATION_TYPE,
                ConfirmationActivity.SUCCESS_ANIMATION);
        intent.putExtra(ConfirmationActivity.EXTRA_MESSAGE,
                getString(R.string.congrats));
        startActivity(intent);
    }

    private void onNextBtClick() {

        final long time = Math.abs(getTime());
        final long timeInMin = time / ONE_MINUTE;
        mStartTime = mStartTime - (time - timeInMin * ONE_MINUTE) + ONE_SECOND;
    }

    private void onPrevBtClick() {

        final long time = Math.abs(getTime());
        final long timeInMin = time / ONE_MINUTE;
        mStartTime = mStartTime + ((timeInMin + 1) * ONE_MINUTE - time) + ONE_SECOND;

    }

    private Timer mTimer = new Timer("RegattaTimer");





    private long getTime() {

        switch ( mState ) {

            case COUNT:
            case COUNTDOWN:
                return System.currentTimeMillis() - mStartTime;

            case COUNTDOWN_PAUSED:
            case COUNT_ENDED:
                return mPauseTime - mStartTime;


            case STOPPED:
            default:
                return -START_PROCEDURE_DURATION;
        }

    }

    private void scheduleRefresh() {

        mTimer.cancel();

        if ((mStartTime != 0) && ((mState == State.COUNTDOWN) || (mState == State.COUNT))) {
            mTimer = new Timer();
            try {
                mTimer.schedule(new RegattaTimerTask(), getDelay());
            } catch (IllegalStateException e) {
                Log.d(TAG, "Scheduling failed ", e);
            }
        }
    }


    private long getDelay() {

        //Log.d(TAG, "getDelay() : isAmbient->"+isAmbient());

        if (isAmbient()) {
            final long time = getTime();
            if (time >= -ONE_MINUTE && time <= 0) { // last minute before start of race
                return ONE_SECOND;
            } else if (time < -ONE_MINUTE) {
                return Math.min(ONE_MINUTE, Math.abs(time) - ONE_MINUTE); // TODO: 12/06/2015 à tester
            } else {
                return ONE_MINUTE;
            }
        } else {
            return ONE_SECOND;
        }
    }


    @Override
    public void onEnterAmbient(Bundle ambientDetails) {
        super.onEnterAmbient(ambientDetails);

        updateUI();
        scheduleRefresh();
    }

    @Override
    public void onUpdateAmbient() {
        Log.d(TAG, "onUpdateAmbient()");
        super.onUpdateAmbient();

        updateUI();
    }

    @Override
    public void onExitAmbient() {
        super.onExitAmbient();

        updateUI();
        scheduleRefresh();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "onPause()");

        mTimer.cancel();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume()");

        updateUI();
        scheduleRefresh();

    }



    private class RegattaTimerTask extends TimerTask {
        @Override
        public void run() {
            MainActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    final long time = getTime();
                    final long timeInSeconds = time / 1000;
                    if ((timeInSeconds >= 0) && (mState == State.COUNTDOWN)) {
                        mState = State.COUNT;
                    }
                    updateUI();
                    scheduleRefresh();
                }
            });
        }
    };
}
