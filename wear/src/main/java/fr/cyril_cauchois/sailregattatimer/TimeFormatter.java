package fr.cyril_cauchois.sailregattatimer;

import android.graphics.Color;

/**
 * Created by FR20093 on 09/06/2015.
 */
public final class TimeFormatter {

    private static final int ONE_SECOND = 1000;
    private static final int SECONDS_PER_MIN = 60;
    private static final int MINUTES_PER_HOUR = 60;
    private static final int SECONDS_PER_HOURS = SECONDS_PER_MIN * MINUTES_PER_HOUR;
    private static final int ONE_MINUTE = SECONDS_PER_MIN * ONE_SECOND;
    private static final int HOURS_PER_DAY = 24;

    private TimeFormatter() {

    }

    public static String formatHMS(long time, boolean isAmbient) {

        final long absTime = Math.abs(time);
        final int sec = ((int) (absTime / ONE_SECOND)) % SECONDS_PER_MIN;
        final int min = ((int) ((absTime / ONE_SECOND)) / SECONDS_PER_MIN) % SECONDS_PER_MIN;
        final int hour = ((int) ((absTime / ONE_SECOND)) / SECONDS_PER_HOURS) % HOURS_PER_DAY;

        if ( time >= 0 ) {
            if ( isAmbient ) {
                return String.format("%02d:%02d", hour, min);
            } else {
                return String.format("%02d:%02d:%02d", hour, min, sec);
            }
        } else {
            return String.format(" - %02d:%02d", min, sec);
        }
    }

    public static int getColor(long time, boolean isAmbient) {

        if ( isAmbient ) {
            return Color.LTGRAY;
        } else {

            if ((time >= -ONE_MINUTE) && (time < 0)) { // last minute
                return Color.RED;
            } else {
                return Color.WHITE;
            }
        }

    }
}
